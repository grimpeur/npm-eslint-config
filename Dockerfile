FROM node:12-alpine AS update
RUN npm install --global --loglevel error npm@6

FROM update AS layout
USER node
WORKDIR /home/node
COPY --chown=node:node ./config ./config
RUN mkdir ./app
WORKDIR ./app

FROM layout AS development
RUN for FILE in ../config/*; do ln -s $FILE ./; done
RUN for FILE in ../config/.[!.]*; do ln -s $FILE ./; done
