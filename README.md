# `@bednarz.dev/eslint-config` [![npm version][npm-image]][npm-url]

> Environment-agnostic ECMAScript 2018 base configuration.
  Extend with module and host object configuration as needed.

## Peer dependencies

- `eslint@6.2`

## Installation

    npm install @bednarz.dev/eslint-config

## Usage

    {
      "extends": "@bednarz.dev"
    }

## License

MIT

[npm-image]: https://img.shields.io/npm/v/@bednarz.dev/eslint-config.svg?style=flat-square
[npm-url]:   https://www.npmjs.com/package/@bednarz.dev/eslint-config
